package com.prueba.encuenstamusical.repository;

import com.prueba.encuenstamusical.model.Encuesta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EncuestaRepository extends JpaRepository<Encuesta,Long> {
    boolean existsByEmail(String email);
}
