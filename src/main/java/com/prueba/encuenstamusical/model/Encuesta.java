package com.prueba.encuenstamusical.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Data
@Entity
@Table(name = "encuesta")
public class Encuesta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "email", nullable = false)
    @Pattern(regexp="^[^@]+@[^@]+\\.[a-zA-Z]{2,}$", message="No se ha escrito en el formato permitido")
    @Email
    private String email;

    @Column(name = "estilo_musical", nullable = false)
    private String estiloMusical;
}
