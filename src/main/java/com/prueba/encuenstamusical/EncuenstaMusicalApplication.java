package com.prueba.encuenstamusical;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EncuenstaMusicalApplication {

	public static void main(String[] args) {
		SpringApplication.run(EncuenstaMusicalApplication.class, args);
	}

}
