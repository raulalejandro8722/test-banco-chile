package com.prueba.encuenstamusical.controller;

import com.prueba.encuenstamusical.model.Encuesta;
import com.prueba.encuenstamusical.model.Mensaje;
import com.prueba.encuenstamusical.service.EncuestaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("api/encuesta")
@CrossOrigin(origins="http://localhost:5200")
public class EncuestaController {

    @Autowired
    private EncuestaService encuestaService;

    @PostMapping("/saveEncuesta")
    public ResponseEntity<?> saveEncuesta(@Valid @RequestBody Encuesta encuesta, Errors errors){
        if(errors.hasErrors()){
            log.error("Error con datos de la encuesta");
            return new ResponseEntity<>(new Mensaje("Error en tipos de datos"),HttpStatus.BAD_REQUEST);
        }
        if(encuestaService.existsByEmail(encuesta.getEmail())) {
            log.error("Ya existe un usuario ese email");
            return new ResponseEntity(new Mensaje("Ya existe un usuario con ese email"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(encuestaService.saveEncuesta(encuesta), HttpStatus.CREATED);
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> getAllEncuesta(){
        return ResponseEntity.ok(encuestaService.getAllEncuestas());
    }
}
