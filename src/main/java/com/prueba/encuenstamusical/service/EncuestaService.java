package com.prueba.encuenstamusical.service;

import com.prueba.encuenstamusical.model.Encuesta;

import java.util.List;

public interface EncuestaService {
    Encuesta saveEncuesta(Encuesta encuesta);

    boolean existsByEmail(String email);

    List<Encuesta> getAllEncuestas();
}
