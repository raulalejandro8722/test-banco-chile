package com.prueba.encuenstamusical.service;

import com.prueba.encuenstamusical.model.Encuesta;
import com.prueba.encuenstamusical.repository.EncuestaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EncuestaServiceImpl implements EncuestaService {

    @Autowired
    private EncuestaRepository encuestaRepository;

    @Override
    public Encuesta saveEncuesta(Encuesta encuesta){
        return encuestaRepository.save(encuesta);
    }

    @Override
    public boolean existsByEmail(String email){
        return encuestaRepository.existsByEmail(email);
    }
    @Override
    public List<Encuesta> getAllEncuestas(){
        return encuestaRepository.findAll();
    }

}
